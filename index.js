
const app = new Vue({
    el: '#app',
    data: {
        message: 'Bloup',
        tab: ['ga', 'zo', 'bu'],
        showList: false
    },
    methods: {
        showBloup() {
            alert(this.message);
        },
        toggleShow() {
            this.showList = !this.showList;
        }
    }
});